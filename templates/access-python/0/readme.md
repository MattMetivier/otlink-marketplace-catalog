# Python

This template creates a Python - Loopedge Access application.

## Usage

### SDK API Key:

SDK Api Key is an API Key to interact with Loopedge Access.

Please check the following steps to generate it:

1. Open Loopedge's web interface, and go to System > Access.
2. Click on "Create Account", name it anything you want, and click "Create Account"
3. Click the vertical ellipsis "⋮" again and click "View API Key", copy it.
   The API key should be in the format:
   
   `01234567-89ab-cdef-fedc-ba9876543210`

### Inbound & Outbound Topic

Inbound & Outbound topics are the topic that allowed to be read and write from internal Loopedge System through Loopedge Access.

Please check the following steps to set the access:

1. Open Loopedge's web interface, and go to System > Access.
2. Create an Account / Open Existing account
3. Beside your new account click the vertical ellipsis "⋮" and click "Edit Topics".
4. Create some topics to read/subscribe to, and also some topics to write/publish to.
 
